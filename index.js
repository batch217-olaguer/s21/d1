console.log("Hola World!");

// [SELECTION] Arrays
// Arrays are used to store multiple related values in a single variable
// Usually we declare an array using square brackets ([]) also known as "Array Literals."

let studentNameA = '2020-1923';
let studentNameB = '2020-1924';
let studentNameC = '2020-1925';
let studentNameD = '2020-1926';

// Declaration of an Array
// SYNTAX --> let/cons arrayName = [elementA, elementB, elementC...];

let studentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926'];

// Common examples of arrays
let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsu"];

// Not recommended use for an array
let mixedArr = [12, "Asus", null, undefined, {}];

console.log(studentNumbers);
console.log(grades);
console.log(computerBrands);
console.log(mixedArr);

//Alternative way of writing an array

let myTasks = [
	"drink html",
	"eat javascript",
	"inhale css",
	"bake sass",
];

// Create an array with values from variable
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Lisbon";

let cities = [city1, city2, city3];

console.log(myTasks);
console.log(cities);

// [SECTION] Length Property
// The .length property allows us to get and aset the total number of items in an array.

// We use .length-1 to delete the last item in an array
let fullName = "Jamie Noble";
console.log(fullName.length);
console.log(cities.length);

// length property can also set the total number of items in an array, meaning we can actually delete the last item in the array or shorten teh array by simply updating the length property of an array.

myTasks.length = myTasks.length - 1;
console.log(myTasks.length);
console.log(myTasks);

cities.length--;
console.log(cities);

// We cannot do the same on strings.
fullName.length = fullName.length-1;
console.log(fullName.length);
fullName.length--;
console.log(fullName);

// you can also lengthen it by adding a number into the length proeprty.

// Adding an item in an array using incrementation (++)
let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length++;
theBeatles.length++;
theBeatles[theBeatles.length] = "Tom";
theBeatles[theBeatles.length] = "Derek";
console.log(theBeatles);

// [SECTION] Reading from Arrays
// Accessing array elements is one of the more common tasks that we do with an array .
// We can access a specific data in an array using an index
// Each element in an array is associated with it's own index/number

console.log(grades[0]);
console.log(computerBrands[3]);

let lakersLegends = [
	"Kobe",
	"Shaq",
	"Lebron",
	"Magic",
	"Kareem",
]
console.log(lakersLegends.length[-1]);
console.log(lakersLegends[4]);

let currentLaker = lakersLegends[2]
console.log(currentLaker);

// You can also reassign array values using the items' indices
console.log("Array before re-assignment");
console.log(lakersLegends);
lakersLegends[2] = "Pau Gasol";
console.log("Array after re-assignment");
console.log(lakersLegends);

// Accessing the last element of an array
let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];
let lastElementIndex = bullsLegends.length-1;
console.log(bullsLegends[lastElementIndex]);

// you can also directly add it.
console.log(bullsLegends[bullsLegends.length-1]);

// Adding items to an array
// Using indeces, you can also add items into the array

let newArr = [];
console.log(newArr);
console.log(newArr[0]);
newArr[0] = "Cloud Strife";
console.log(newArr);

console.log(newArr[1]);
newArr[1] = "Tifa Lockheart";
console.log(newArr);

// You can also add items at the end of the array. Instead of adding it in the fornt to avoid the risk of replacing the first item in the array.
// newArr[newArr.length-1] = "Aerith Gainsborough"; - will overwrite the last item instead.

newArr[newArr.length] = "Barett Wallace";
console.log(newArr);

// Looping over an array
// You can use a for loop to iterate over all items in an array.
// Set the counter as the index and set the conditio that as long as the current index iterated is less than the length of an array.

for (let index = 0; index < newArr.length; index++){
	// you can use the loop counter to be able to show items in the console.
	console.log(newArr[index]);
}

let numArr = [5, 12, 30, 46, 40];

numArr[numArr.length] = 14;
numArr[numArr.length] = 23;
numArr[numArr.length] = 4;
console.log(numArr);

for (let index = 0; index < numArr.length; index++) {
	
	if(numArr[index] % 5 === 0){
		console.log(numArr[index] + " is divisible by 5.")
	} else {
		console.log(numArr[index] + " is not divisible by 5.")
	}
}

// [SECTION] Multidimensional Array

/*
	- Mutidimensional array.


*/

let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log(chessBoard);
console.log(chessBoard[1][4])
// [1] --> column
// [4] --> row
// console.log(chessBoard[column][row])
console.log(chessBoard[1][4]);
console.log(chessBoard[4][3]);

console.table(chessBoard);
console.log("Pawn moves to: "+ chessBoard[1][5]);